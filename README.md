# Estandar para mensajes de commit

- La estandarización de los mensajes en los commits es sumamente importante ya que
es la mejor manera de comunicar el contexto del cambio realizado al resto de los
desarrolladores que trabajan en el proyecto. Esta estandarización se logrará a través
de un archivo template predeterminado, el cual será la guía para escribir el mensaje en
nuestros commits.

13/10/2021 by **Nova Solution Systems**


##### Estructura convencional para un mensaje de commit
```bash
<type>: <description>

[optional body]

[optional footer(s)]
```

## Instalación y Configuración

### Template gitmessage
Template estandar para la escritura del mensaje de commit.

1. Copie el archivo [gitmessage](./templates/gitmessage.txt) a su directorio ***$HOME*** ejecutando;

```bash
[developer@nova]$ cp gitmessage.txt $HOME/.gitmessage.txt
```

2. Agregue el template a la configuración de git, ejecutando:
```bash
[developer@nova]$ git config --global commit.template $HOME/.gitmessage.txt
```

### Script commit-msg
Script que valida que el mensaje del commit cumpla con la estructura convencional.

#### A nivel repositorio
1. Copie el archivo [commit-msg](./scripts/commit-msg) al directorio hooks de su repositorio, ejecutando:
```bash
[developer@nova]$ cp commit-msg ${YOUR_AWESOME_PROJECT}/.git/hooks/
```

2. Asigne permisos de ejecución al script commit-msg.
```bash
[developer@nova]$ chmod +x ${YOUR_AWESOME_PROJECT}/.git/hooks/commit-msg
```

#### A nivel global
1. Cree un directorio para alojar los hooks globales, ejecutando:
```bash
[developer@nova]$ mkdir -p $HOME/.git-templates/hooks
```
2. Copie el archivo [commit-msg](./scripts/commit-msg) al directorio previamente creado, ejecutando:
```bash
[developer@nova]$ cp commit-msg $HOME/.git-templates/hooks/commit-msg
```
3. Asigne permisos de ejecución al script commit-msg.
```bash
[developer@nova]$ chmod +x $HOME/.git-templates/hooks/commit-msg
```
4. Agregue el directorio *git-templates* a la configuración global de git init, ejecutando:
```bash
[developer@nova]$ git config --global init.templatedir $HOME/.git-templates
```
Esto permite que cada que se inicialize un nuevo repositorio, se carguen los scripts necesarios dentro del directorio *.git* del repositorio.

**Nota:** Para hacer uso del script de validación en los repositorios ya existentes, es necesario ejecutar el subcomando ***git init*** dentro del directorio del repositorio.

## Las 7 reglas de un gran mensaje de commit

1. Separa el asunto del cuerpo con una línea en blanco
2. Limita la línea de asunto a 50 caracteres
3. Capitaliza la línea de asunto
4. No termines el asunto con un punto
5. Utiliza el modo imperativo en el asunto

    Una línea de asunto propiamente formada de git commit siempre debe poder
    completar la siguiente oración:<br>
    *Si se aplica, este commit **<aquí va la línea de asunto>***<br>
    
    Por ejemplo:
    - *Si se aplica, este commit elimina los métodos obsoletos*
    - *Si se aplica, este commit lanza la versión 2.0*

    **Nota**: El uso del imperativo es importante solo en la línea del asunto, puedes
    omitir esta regla al momento de redactar el cuerpo del mensaje.
6. Envuelve el cuerpo del mensaje en 72 caracteres
7. Utiliza el cuerpo para explicar qué y por qué vs cómo<br>

    En la mayoría de los casos, se pueden omitir los detalles sobre cómo se realizó
    un cambio.
    Al momento de escribir el cuerpo del mensaje solo se debe de concentrar en
    aclarar las razones principales del por qué hizo el cambio, la forma en la que las
    cosas funcionaban antes del cambio (y qué estaba mal en eso), finalizando con
    la forma en la que las cosas funcionan ahora y por qué decidió resolverlo de la
    forma en la que la hizo.


**Tabla 1.** Tipos de cambios aceptados.

| Tipo        | Descripción | 
|:-------:    |:--------:|          
| feat        | Nueva característica | 
| fix         | Corrección de bugs/errores. | 
| Style       | Aplicación de formato, puntos y comas faltantes, etc; sin cambio de código productivo|
| refactor    | Refactorización de código.|
| perf        | Mejoras de rendimiento |
| test        | Agregar o refactorizar pruebas; sin cambio de código productivo |
| docs        | Cambios en la documentación |
| chore       | Mantenimiento regular del código |
| version     | Aumento de versión/ nuevo release; sin cambio de código productivo |
| defaults    | Cambiar las opciones predeterminadas |


